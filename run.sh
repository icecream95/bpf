#!/bin/bash

GALLIUM_DRI=/tmp/panfrost/lib/dri/panfrost_dri.so
EMACS=/gnu/store/zv7cb4d43csbj03fxjxv7vpwniggyvr1-emacs-no-x-27.2/bin/.emacs-27.2-real

SCRIPT="$1"
shift 1

if ! [ -e "$SCRIPT" ]; then
    printf "USAGE: %s BPFTRACE_SCRIPT\n" "$0"
    exit
fi

if ! [ "$UID" = 0 ]; then
    hash doas 2>/dev/null && exec doas "$0" "$SCRIPT" "$@"
    hash sudo 2>/dev/null && exec sudo "$0" "$SCRIPT" "$@"
    printf "This script must be run as root\n"
    exit 1
fi

if ! [ -r /sys/kernel/debug/ ]; then
    mount -t debugfs debug /sys/kernel/debug
fi

SCRIPT_DIR="$(dirname "$(realpath -- "$0")")"
INCLUDE_DIR="$SCRIPT_DIR"/include

cleanup() {
    rm -- "$TEMP_FILE"
}

if grep -q 'GALLIUM_DRI\|EMACS' "$SCRIPT"; then
    TEMP_FILE="$(mktemp)"
    trap cleanup EXIT

    awk -v dri="$GALLIUM_DRI" -v emacs="$EMACS" -- '

/GALLIUM_DRI/ { gsub("GALLIUM_DRI", dri) }
/EMACS/ { gsub("EMACS", emacs) }

{ print }
' "$SCRIPT" >"$TEMP_FILE"

    SCRIPT="$TEMP_FILE"
fi

bpftrace -I "$INCLUDE_DIR" "$SCRIPT"
